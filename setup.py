from io import open
from os import path

from setuptools import find_packages, setup

with open(path.join("README.md"), encoding="utf-8") as f:
    long_description = f.read()

setup(
    name="webSearchCatalog",
    version="0.1.0",
    description=long_description,
    classifiers=[
        "Programming Language :: Python :: 3",
    ],
    packages=find_packages(),
    python_requires=">=3",
    install_requires=[
        "appdirs==1.4.4",
        "asgiref==3.4.1",
        "black==21.7b0",
        "certifi==2021.5.30",
        "charset-normalizer==2.0.3",
        "click==8.0.1",
        "ecdsa==0.17.0",
        "elasticsearch[async]==7.13.4",
        "fastapi==0.67.0",
        "h11==0.12.0",
        "httptools==0.2.0",
        "idna==3.2",
        "mypy-extensions==0.4.3",
        "numpy==1.21.1",
        "pathspec==0.9.0",
        "pyasn1==0.4.8",
        "pydantic==1.8.2",
        "pygeos==0.10.1",
        "python-dotenv==0.18.0",
        "python-jose==3.3.0",
        "python-keycloak==0.25.0",
        "PyYAML==5.4.1",
        "regex==2021.7.6",
        "requests==2.26.0",
        "rsa==4.7.2",
        "six==1.16.0",
        "starlette==0.14.2",
        "tomli==1.1.0",
        "typing-extensions==3.10.0.0",
        "urllib3==1.26.6",
        "uvicorn==0.14.0",
        "uvloop==0.15.3",
        "watchgod==0.7",
        "websockets==9.1",
        "geomet==0.3.0",
    ],
)
