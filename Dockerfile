FROM gl-registry.ext-dc1.o.nkpor.etris/adm/images/centosbasepython/centos_base_python:latest-py37
#
# Consul settings
ENV ENVIRONMENT=development \
    APPID=web-search-catalog \
    HOST_IP="http://consul01.ext-dc1.o.nkpor.etris/"

ADD admFiles/*.hcl    /etc/consul/
ADD admFiles/env.sh   /tmp/
ADD admFiles/pip.conf /root/.pip/pip.conf
ADD admFiles/pypirc   /root/.pypirc

RUN set -xv ;\
    yum makecache ;\
    yum install -y gcc-c++ make curl openssl ;\
    yum clean all ;\
    rm -rf /var/cache/yum/ ;\
    localedef -i ru_RU -f UTF-8 ru_RU.utf ;\
    source /tmp/env.sh ;\
    pip3 install --no-cache-dir ${PACKAGE_NAME}==${PACKAGE_VERSION} ;\
#
# consul settings
    SHARE="http://nexus.ext-dc1.o.nkpor.etris:8081/repository/share/consul" ;\
    FILENAME="consul-template_0.25.0_linux_amd64.tgz" ;\
    curl --silent --output ${FILENAME} ${SHARE}/${FILENAME} ;\
    tar xvzf ${FILENAME} -C /usr/bin/ ;\
    rm -f ${FILENAME} ;\
#
    python  --version ;\
    python3 --version ;\
    pip3    --version ;\
    pip3 list
#
ENTRYPOINT /usr/bin/consul-template -consul-addr "${HOST_IP}:8500" -config /etc/consul/config.hcl
#
