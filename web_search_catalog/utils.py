from datetime import datetime

import requests
from elasticsearch import AsyncElasticsearch
from geomet import wkt

from web_search_catalog.constans import (
    COVERAGE_FIELDS,
    BbpConstans,
    CustomFields,
    ElasticConstants,
)
from web_search_catalog.settings import config


class ElasticUtil:
    def __init__(self, host, port):
        self.es = AsyncElasticsearch(
            [
                {
                    "host": host,
                    "port": port,
                }
            ],
        )

    async def search(self, body):
        body_to_es = self.get_search_body(
            {k: v for k, v in body.dict().items() if v is not None}
        )
        response = await self.es.search(
            filter_path=["hits.hits._source", "hits.hits.inner_hits"],
            index=config["elasticsearch"]["index"],
            body=body_to_es,
            _source_excludes="@version,@timestamp",
            size=body.size,
            request_timeout=config["elasticsearch"]["request_timeout"],
            _source_includes=self.get_source_includes(body) if body.fields else None,
        )
        if not response:
            return []
        result = []
        for image in response["hits"]["hits"]:
            res = image["_source"]
            if body.fields and "coverage" in body.fields or not body.fields:
                if image.get("inner_hits"):
                    res["coverage"] = [
                        coverage["_source"]
                        for coverage in image["inner_hits"]["coverage"]["hits"]["hits"]
                    ]
            result.append(res)
        return result

    def get_search_body(self, body):
        es_body, bool_dict, must_list = {}, {}, []
        must_list.extend(self.get_match_params_body(body))
        must_list.extend(self.get_terms_params_body(body))
        must_list.extend(self.get_range_params_body(body))
        # 12312
        if body.get("coverage_exists"):
            must_list.append(self.get_coverage_field())
        must_list = list(filter(None, must_list))
        bool_dict["must"] = must_list
        bool_dict["must_not"] = ElasticConstants.BASE_MUST_NOT_PARAMS
        es_body["query"] = {
            "bool": bool_dict,
        }
        if body.get("geometry"):
            bool_dict["filter"] = {
                "geo_shape": {
                    "geometry": {
                        "shape": body["geometry"],
                        "relation": "intersects",
                    }
                }
            }
        if body.get("ordering") and body.get("ordering").value.replace("-", "") not in [
            field.value for field in CustomFields
        ]:
            es_body["sort"] = {
                body["ordering"].value.replace("-", ""): {
                    "order": "desc" if body["ordering"].value[0] == "-" else "asc"
                }
            }
        return es_body

    def get_match_params_body(self, body):
        intersect_dict = {
            param: body[param]
            for param in body.keys() & ElasticConstants.MATCH_PARAMS_MAPPING.keys()
        }
        match_list = [ElasticConstants.BASE_MATCH_PARAMS]
        for key, value in intersect_dict.items():
            match_list.append(
                {"match": {ElasticConstants.MATCH_PARAMS_MAPPING[key]: value}}
            )
        return match_list if match_list else None

    def get_terms_params_body(self, body):
        intersect_dict = {
            param: body[param]
            for param in body.keys() & ElasticConstants.TERMS_PARAMS_MAPPING.keys()
        }
        terms_list = [ElasticConstants.BASE_TERMS_PARAMS]
        for key, value in intersect_dict.items():
            terms_list.append(
                {
                    "terms": {
                        ElasticConstants.TERMS_PARAMS_MAPPING[key]: value.split(",")
                    }
                }
            )
        return terms_list if terms_list else None

    def get_range_params_body(self, body):
        intersect_dict = {
            param: body[param]
            for param in body.keys() & ElasticConstants.RANGE_PARAMS_MAPPING.keys()
        }
        range_list = [ElasticConstants.BASE_RANGE_PARAMS]
        for key, value in intersect_dict.items():
            range_list.append(
                {
                    "range": {
                        ElasticConstants.RANGE_PARAMS_MAPPING[key][0]: {
                            ElasticConstants.RANGE_PARAMS_MAPPING[key][1]: value
                        }
                    }
                }
            )
        return range_list if range_list else None

    def get_sort_field(self, field):
        return {field.replace("-", ""): {"order": "desc" if field[0] == "-" else "asc"}}

    def get_source_includes(self, body):
        result = body.fields
        for field in ElasticConstants.REQUIRED_FIELDS:
            if getattr(body, field) and field not in body.fields.split(","):
                result += f",{field},"
        return result

    def get_coverage_field(self):
        return {
            "has_child": {
                "type": "coverage",
                "query": {"match_all": {}},
                "inner_hits": {"_source": {"includes": COVERAGE_FIELDS}},
            }
        }

    async def close(self):
        await self.es.close()


class BbpUtil:
    def __init__(self, search_endpoint, api_key):
        self.search_endpoint = search_endpoint
        self.api_key = api_key

    def search(self, body):
        search_body = {k: v for k, v in body.dict().items() if v is not None}
        a = self.get_search_body(search_body)
        response = requests.post(
            f"{self.search_endpoint}?api_key={self.api_key}",
            json=self.get_search_body(search_body),
        )
        print(a)
        if response.status_code != 200:
            raise Exception("Ошибка при обращении к BBP")
        return self.get_response_body(response.json(), body.fields)

    def get_search_body(self, body):
        base = BbpConstans.BASE_SEARCH_BODY.copy()
        for key, value in body.items():
            if BbpConstans.REQUEST_MAPPING.get(key):
                mapped_field = BbpConstans.REQUEST_MAPPING[key]
                if not base.get(mapped_field):
                    base[mapped_field] = getattr(
                        self, f"get_request_field_{mapped_field}"
                    )(body)
        return base

    def get_request_field_sort(self, body):
        if body.get("ordering").value.replace("-", "") not in [
            field.value for field in CustomFields
        ]:
            return [
                {
                    BbpConstans.SORT_MAPPING[
                        body["ordering"].value.replace("-", "")
                    ]: "desc"
                    if body["ordering"].value[0] == "-"
                    else "asc"
                }
            ]

    def get_request_field_processing_level(self, body):
        return body["processing_level_code"]

    def get_request_platform_type_identifier(self, body):
        result = []
        for platform_id in body["platform_type_identifier"].split(","):
            result.extend(
                [
                    key
                    for key, value in BbpConstans.PLATFORM_TYPE_IDENTIFIER_MAPPING.items()
                    if value == platform_id
                ]
            )
        return result

    def get_request_field_fields(self, body):
        result = list(
            filter(
                lambda x: x not in [field.value for field in CustomFields],
                body["fields"].split(","),
            )
        )
        invert_mapping = {v: k for k, v in BbpConstans.RESPONSE_MAPPING.items()}
        return list(map(lambda x: invert_mapping.get(x), result))

    def get_request_field_geometry_object(self, body):
        return {"type": "Feature", "geometry": wkt.loads(body["geometry"])}

    def get_request_field_platform_id(self, body):
        return body["platform_identifiers"].split(",")

    def get_request_field_acquisition_date(self, body):
        return [
            body["acquisition_date_after"]
            if body.get("acquisition_date_after")
            else "1970-01-01T00:00:00Z",
            body["acquisition_date_before"]
            if body.get("acquisition_date_before")
            else datetime.now().isoformat(),
        ]

    def get_request_field_cloud_cover(self, body):
        return [0, body["cloudiness_max"]]

    def get_request_field_sensor_id(self, body):
        return body["instrument_identifiers"].split(",")

    def get_request_field_scene_id(self, body):
        return body["identifier"][4:]

    def get_response_body(self, body, fields):
        new_result = []
        field_by_funcs = list(filter(lambda x: "get_field_" in x, dir(self)))
        if fields:
            field_by_funcs = list(
                filter(lambda x: x.split("get_field_")[1] in fields, field_by_funcs)
            )
        for scene in body["result"]:
            new_scene = {}
            for key, value in scene.items():
                if BbpConstans.RESPONSE_MAPPING.get(key):
                    new_scene[BbpConstans.RESPONSE_MAPPING[key]] = value
            for func in field_by_funcs:
                new_scene[func.split("get_field_")[1]] = getattr(self, func)(scene)
            if not fields:
                for field in BbpConstans.CONST_FIELDS:
                    new_scene.update(field)
            new_result.append(new_scene)
        return new_result

    def get_field_illumination_elevation_angle(self, scene):
        return 90 - scene["sun_zenith_angle"] if scene.get("sun_zenith_angle") else None

    def get_field_platform_type_identifier(self, scene):
        return BbpConstans.PLATFORM_TYPE_IDENTIFIER_MAPPING[scene["platform_id"]]

    def get_field_sensors(self, scene):
        return BbpConstans.SENSORS_MAPPING[scene["sensor_id"]]

    def get_field_identifier(self, scene):
        return "BBP." + scene["scene_id"]

    def get_field_geometry(self, scene):
        return wkt.dumps(scene["bounding_shape"]["geometry"], decimals=6)[11:]

    def get_field_previews(self, scene):
        return [{"url": scene["browseimage"]["EPSG:3857"]}]

    def get_field_last_modified(self, scene):
        return scene["modification_date"]
