from typing import Optional

from pydantic import BaseModel, Field, validator
from pygeos import Geometry, GEOSException, get_type_id, is_valid

from web_search_catalog.constans import OrderingFields


class SearchBody(BaseModel):
    identifier: Optional[str] = None
    platform_identifiers: Optional[str] = None
    platform_type_identifier: Optional[str] = None
    instrument_identifiers: Optional[str] = None
    processing_level_code: Optional[str] = None
    geometry: Optional[str] = None
    size: Optional[int] = Field(100, gt=0, le=10000)
    ordering: Optional[OrderingFields] = OrderingFields.acquisition_date_begin_desc
    cloudiness_max: Optional[int] = Field(None, ge=0, le=100)
    fields: Optional[str] = None
    acquisition_date_after: Optional[str] = None
    acquisition_date_before: Optional[str] = None
    coverage_exists: Optional[bool] = False

    @validator("geometry")
    def roi_validate(cls, value):
        try:
            roi = Geometry(value)
        except GEOSException:
            raise ValueError("Bad geometry")
        if get_type_id(roi) != 3:  # 3 --> Polygon
            raise ValueError("Geometry must be Polygon")
        if not is_valid(roi):
            raise ValueError("Invalid Polygon")
        return value
