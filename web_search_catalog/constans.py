from enum import Enum


# Mapping WebSearchCatalog -> ES
class ElasticConstants:
    """Константы для формирования запроса для ES"""

    #  Базовые параметры, которые должны быть применены всегда
    BASE_MATCH_PARAMS = {}
    BASE_TERMS_PARAMS = {}
    BASE_RANGE_PARAMS = {}
    BASE_MUST_NOT_PARAMS = {"exists": {"field": "pole_area_intersection"}}

    #  https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-match-query.html
    MATCH_PARAMS_MAPPING = {
        "identifier": "identifier",
        "processing_level_code": "processing_level_code",
    }

    #  https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-terms-query.html
    TERMS_PARAMS_MAPPING = {
        "platform_identifiers": "platform_identifier.keyword",
        "platform_type_identifier": "platform_type_identifier.keyword",
        "instrument_identifiers": "instrument_identifier.keyword",
    }

    #  https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-range-query.html
    RANGE_PARAMS_MAPPING = {
        "acquisition_date_after": ("acquisition_date_begin", "gte"),
        "acquisition_date_before": ("acquisition_date_begin", "lte"),
        "cloudiness_max": ("cloudiness", "lte"),
    }

    #  Обязательные для возвращения из ES поля В СЛУЧАЕ их использования в теле запроса
    REQUIRED_FIELDS = ("geometry",)


# Поля в теле ответа, генерируемые самим приложением
class CustomFields(Enum):
    intersection_pct = "intersection_pct"


# Поля, по которым можно осуществлять сортировку
class OrderingFields(Enum):
    intersection_pct_asc = "intersection_pct"
    intersection_pct_desc = "-intersection_pct"
    cloudiness_asc = "cloudiness"
    cloudiness_desc = "-cloudiness"
    acquisition_date_begin_asc = "acquisition_date_begin"
    acquisition_date_begin_desc = "-acquisition_date_begin"
    identifier_asc = "identifier"
    identifier_desc = "-identifier"
    resolution_asc = "resolution"
    resolution_desc = "-resolution"
    id_asc = "id"
    id_desc = "-id"
    last_modified_asc = "last_modified"
    last_modified_desc = "-last_modified"
    instrument_identifier_asc = "instrument_identifier"
    instrument_identifier_desc = "-instrument_identifier"


# Поля, которые присутствуют в поле coverage
COVERAGE_FIELDS = [
    "processing_level_code",
    "service_name",
    "created",
    "resolution",
    "acquisition_date",
    "platform_identifier",
    "path",
    "metadata_identifier",
    "name",
    "instrument_identifier",
    "geometry",
    "id",
    "last_modified",
    "format_name",
    "repository_name",
    "_id",
    "resource_urls",
]


class BbpConstans:
    SENSORS_MAPPING = {
        "MSUTM101": [
            {
                "instrument_identifier": "MSUTM101",
                "bands": [
                    {
                        "id": "MSUTM101.1",
                        "name": "RED",
                        "description": "RED",
                        "min": 0.64,
                        "max": 0.69,
                    },
                    {
                        "id": "MSUTM101.2",
                        "name": "NIR",
                        "description": "NIR",
                        "min": 0.785,
                        "max": 0.9,
                    },
                    {
                        "id": "MSUTM101.3",
                        "name": "GREEN",
                        "description": "GREEN",
                        "min": 0.52,
                        "max": 0.59,
                    },
                ],
                "is_panchromatic": False,
            }
        ],
        "MSUTM102": [
            {
                "instrument_identifier": "MSUTM102",
                "bands": [
                    {
                        "id": "MSUTM102.1",
                        "name": "RED",
                        "description": "RED",
                        "min": 0.64,
                        "max": 0.69,
                    },
                    {
                        "id": "MSUTM102.2",
                        "name": "NIR",
                        "description": "NIR",
                        "min": 0.785,
                        "max": 0.9,
                    },
                    {
                        "id": "MSUTM102.3",
                        "name": "GREEN",
                        "description": "GREEN",
                        "min": 0.52,
                        "max": 0.59,
                    },
                ],
                "is_panchromatic": False,
            }
        ],
        "KVIK": [
            {
                "instrument_identifier": "MSI",
                "bands": [
                    {
                        "id": "MSI.1",
                        "name": "COASTAL_AEROSOL",
                        "description": "COASTAL_AEROSOL",
                        "min": 0.4304,
                        "max": 0.4574,
                    },
                    {
                        "id": "MSI.10",
                        "name": "CIRRUS",
                        "description": "CIRRUS",
                        "min": 1.336,
                        "max": 1.411,
                    },
                    {
                        "id": "MSI.11",
                        "name": "SWIR",
                        "description": "SWIR",
                        "min": 1.5422,
                        "max": 1.6852,
                    },
                    {
                        "id": "MSI.12",
                        "name": "SWIR",
                        "description": "SWIR",
                        "min": 2.0814,
                        "max": 2.3234,
                    },
                    {
                        "id": "MSI.13",
                        "name": "NIR",
                        "description": "NIR",
                        "min": 0.8483,
                        "max": 0.8813,
                    },
                    {
                        "id": "MSI.2",
                        "name": "BLUE",
                        "description": "BLUE",
                        "min": 0.4476,
                        "max": 0.5456,
                    },
                    {
                        "id": "MSI.3",
                        "name": "GREEN",
                        "description": "GREEN",
                        "min": 0.5375,
                        "max": 0.5825,
                    },
                    {
                        "id": "MSI.4",
                        "name": "RED",
                        "description": "RED",
                        "min": 0.6455,
                        "max": 0.6835,
                    },
                    {
                        "id": "MSI.5",
                        "name": "RED_EDGE",
                        "description": "RED_EDGE",
                        "min": 0.6944,
                        "max": 0.7134,
                    },
                    {
                        "id": "MSI.6",
                        "name": "RED_EDGE",
                        "description": "RED_EDGE",
                        "min": 0.7312,
                        "max": 0.7492,
                    },
                    {
                        "id": "MSI.7",
                        "name": "RED_EDGE",
                        "description": "RED_EDGE",
                        "min": 0.7685,
                        "max": 0.7965,
                    },
                    {
                        "id": "MSI.8",
                        "name": "NIR",
                        "description": "NIR",
                        "min": 0.7626,
                        "max": 0.9076,
                    },
                    {
                        "id": "MSI.9",
                        "name": "NIR",
                        "description": "NIR",
                        "min": 0.932,
                        "max": 0.958,
                    },
                ],
                "is_panchromatic": False,
            }
        ],
        "MSUMR": [
            {
                "instrument_identifier": "MSUMR",
                "bands": [
                    {
                        "id": "MSUMR.1",
                        "name": "RED",
                        "description": "RED",
                        "min": 0.5,
                        "max": 0.7,
                    },
                    {
                        "id": "MSUMR.2",
                        "name": "NIR",
                        "description": "NIR",
                        "min": 0.7,
                        "max": 1.1,
                    },
                    {
                        "id": "MSUMR.3",
                        "name": "SWIR",
                        "description": "SWIR",
                        "min": 1.6,
                        "max": 1.8,
                    },
                    {
                        "id": "MSUMR.4",
                        "name": "MIR",
                        "description": "MIR",
                        "min": 3.5,
                        "max": 4.1,
                    },
                    {
                        "id": "MSUMR.5",
                        "name": "TIR",
                        "description": "TIR",
                        "min": 10.5,
                        "max": 11.5,
                    },
                    {
                        "id": "MSUMR.6",
                        "name": "TIR",
                        "description": "TIR",
                        "min": 11.5,
                        "max": 12.5,
                    },
                ],
                "is_panchromatic": False,
            }
        ],
        "OLITIRS": [
            {
                "instrument_identifier": "OLITIRS",
                "bands": [
                    {
                        "id": "OLITIRS.1",
                        "name": "COASTAL_AEROSOL",
                        "description": "COASTAL_AEROSOL",
                        "min": 0.435,
                        "max": 0.451,
                    },
                    {
                        "id": "OLITIRS.10",
                        "name": "TIR",
                        "description": "TIR",
                        "min": 10.6,
                        "max": 11.19,
                    },
                    {
                        "id": "OLITIRS.11",
                        "name": "TIR",
                        "description": "TIR",
                        "min": 11.5,
                        "max": 12.51,
                    },
                    {
                        "id": "OLITIRS.2",
                        "name": "BLUE",
                        "description": "BLUE",
                        "min": 0.452,
                        "max": 0.512,
                    },
                    {
                        "id": "OLITIRS.3",
                        "name": "GREEN",
                        "description": "GREEN",
                        "min": 0.533,
                        "max": 0.59,
                    },
                    {
                        "id": "OLITIRS.4",
                        "name": "RED",
                        "description": "RED",
                        "min": 0.636,
                        "max": 0.673,
                    },
                    {
                        "id": "OLITIRS.5",
                        "name": "NIR",
                        "description": "NIR",
                        "min": 0.851,
                        "max": 0.879,
                    },
                    {
                        "id": "OLITIRS.6",
                        "name": "SWIR",
                        "description": "SWIR",
                        "min": 1.566,
                        "max": 1.651,
                    },
                    {
                        "id": "OLITIRS.7",
                        "name": "SWIR",
                        "description": "SWIR",
                        "min": 2.107,
                        "max": 2.294,
                    },
                    {
                        "id": "OLITIRS.8",
                        "name": "PAN",
                        "description": "PAN",
                        "min": 0.503,
                        "max": 0.676,
                    },
                    {
                        "id": "OLITIRS.9",
                        "name": "CIRRUS",
                        "description": "CIRRUS",
                        "min": 1.363,
                        "max": 1.384,
                    },
                ],
                "is_panchromatic": False,
            }
        ],
        "MSI": [
            {
                "instrument_identifier": "MSI",
                "bands": [
                    {
                        "id": "MSI.1",
                        "name": "COASTAL_AEROSOL",
                        "description": "COASTAL_AEROSOL",
                        "min": 0.4304,
                        "max": 0.4574,
                    },
                    {
                        "id": "MSI.10",
                        "name": "CIRRUS",
                        "description": "CIRRUS",
                        "min": 1.336,
                        "max": 1.411,
                    },
                    {
                        "id": "MSI.11",
                        "name": "SWIR",
                        "description": "SWIR",
                        "min": 1.5422,
                        "max": 1.6852,
                    },
                    {
                        "id": "MSI.12",
                        "name": "SWIR",
                        "description": "SWIR",
                        "min": 2.0814,
                        "max": 2.3234,
                    },
                    {
                        "id": "MSI.13",
                        "name": "NIR",
                        "description": "NIR",
                        "min": 0.8483,
                        "max": 0.8813,
                    },
                    {
                        "id": "MSI.2",
                        "name": "BLUE",
                        "description": "BLUE",
                        "min": 0.4476,
                        "max": 0.5456,
                    },
                    {
                        "id": "MSI.3",
                        "name": "GREEN",
                        "description": "GREEN",
                        "min": 0.5375,
                        "max": 0.5825,
                    },
                    {
                        "id": "MSI.4",
                        "name": "RED",
                        "description": "RED",
                        "min": 0.6455,
                        "max": 0.6835,
                    },
                    {
                        "id": "MSI.5",
                        "name": "RED_EDGE",
                        "description": "RED_EDGE",
                        "min": 0.6944,
                        "max": 0.7134,
                    },
                    {
                        "id": "MSI.6",
                        "name": "RED_EDGE",
                        "description": "RED_EDGE",
                        "min": 0.7312,
                        "max": 0.7492,
                    },
                    {
                        "id": "MSI.7",
                        "name": "RED_EDGE",
                        "description": "RED_EDGE",
                        "min": 0.7685,
                        "max": 0.7965,
                    },
                    {
                        "id": "MSI.8",
                        "name": "NIR",
                        "description": "NIR",
                        "min": 0.7626,
                        "max": 0.9076,
                    },
                    {
                        "id": "MSI.9",
                        "name": "NIR",
                        "description": "NIR",
                        "min": 0.932,
                        "max": 0.958,
                    },
                ],
                "is_panchromatic": False,
            }
        ],
        "PSS": [],
        "MSS": [],
        "SVR": [],
        "CSAREW": [],
        "CSARIW": [],
    }

    PLATFORM_TYPE_IDENTIFIER_MAPPING = {
        "MM1": "MM",
        "MM2": "MM",
        "MM22": "MM",
        "LS8": "LS8",
        "SNL2A": "SNL2",
        "SNL2B": "SNL2",
        "KVIK": None,
        "RP1": None,
        "RP2": None,
        "BKA": None,
        "SNL1A": None,
        "KV1": None,
        "SNL1B": None,
    }

    #  BBP -> WebSearchCatalog
    RESPONSE_MAPPING = {
        "bounding_shape": "geometry",
        "cloud_cover": "cloudiness",
        "scene_id": "id",
        "catalogization_date": "date_time_stamp",
        "acquisition_date_start": "acquisition_date_begin",
        "acquisition_date_stop": "acquisition_date_end",
        "platform_id": "platform_identifier",
        "sensor_id": "instrument_identifier",
        "resolution": "resolution",
        "orbit_pass": "circuit_number",
        "processing_level": "processing_level_code",
        "browse_image": "previews",
        "frame": "frame",
        "sun_azimuth_angle": "illumination_azimuth_angle",
        "sensor_azimuth_angle": "azimuth_scan_angle",
        "available_to_order": "available_to_order",
        "route": "scan_number",
        "items": "size",
        "sort": "ordering",
        "coverage_exists": "coverage_exists",
    }

    REQUEST_MAPPING = {
        "acquisition_date_after": "acquisition_date",
        "acquisition_date_before": "acquisition_date",
        "cloudiness_max": "cloud_cover",
        "identifier": "scene_id",
        # "platform_type_identifier": "platform_id",
        "platform_identifiers": "platform_id",
        "geometry": "geometry_object",
        "processing_level_code": "processing_level",
        "instrument_identifiers": "sensor_id",
        "ordering": "sort",
        "fields": "fields",
    }

    SORT_MAPPING = {
        "cloudiness": "cloud_cover",
        "resolution": "resolution",
        "identifier": "scene_id",
        "platform_type_identifier": "platform_id",
        "id": "scene_id",
        "last_modified": "modification_date",
        "instrument_identifier": "sensor_id",
        "acquisition_date_begin": "acquisition_date",
    }

    CONST_FIELDS = [
        {
            "resp_party_data_storage": [
                {
                    "organization_name": 'Научный центр оперативного мониторинга Земли ОАО "Российские космические системы"',
                    "url": "http://www.ntsomz.ru",
                }
            ]
        },
        {"nadir_tilt_angle": None},
    ]

    BASE_SEARCH_BODY = {}
