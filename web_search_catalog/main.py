from decimal import Decimal

import uvicorn
from fastapi import Depends, FastAPI, Request
from fastapi.exceptions import RequestValidationError
from pydantic import ValidationError
from pygeos import Geometry, area, intersection, intersects

from web_search_catalog.constans import CustomFields
from web_search_catalog.dependencies import get_user_params
from web_search_catalog.schemas import SearchBody
from web_search_catalog.settings import config
from web_search_catalog.utils import BbpUtil, ElasticUtil

app = FastAPI()


@app.get("/ht")
def ht():
    return {"detail": "ok"}


@app.post("/bbp/scenes")
def bbp_search(search_body: SearchBody):
    result = BbpUtil(config["bbp"]["search_endpoint"], config["bbp"]["api_key"]).search(
        search_body
    )
    return generate_response(search_body, result)


@app.get("/search", dependencies=[Depends(get_user_params)])
async def get_search(request: Request):
    from datetime import datetime

    print(datetime.now().isoformat())
    q_params = request.query_params
    try:
        search_body = SearchBody(**q_params)
    except ValidationError as err:
        raise RequestValidationError(err.raw_errors)
    result = await ElasticUtil(
        config["elasticsearch"]["host"], config["elasticsearch"]["port"]
    ).search(search_body)
    return generate_response(search_body, result)


@app.post("/search", dependencies=[Depends(get_user_params)])
async def post_search(search_body: SearchBody):
    search_result = await ElasticUtil(
        config["elasticsearch"]["host"], config["elasticsearch"]["port"]
    ).search(search_body)
    return generate_response(search_body, search_result)


def generate_response(search_body, search_result):
    if search_body.geometry:
        search_result = get_intersection_pct_field(search_body, search_result)
        return {"count": len(search_result), "results": search_result}
    return {"count": len(search_result), "results": search_result}


def get_intersection_pct_field(body, result):
    for image in result:
        if intersects(Geometry(image["geometry"]), Geometry(body.geometry)):
            a = Decimal(
                area(intersection(Geometry(image["geometry"]), Geometry(body.geometry)))
                / area(Geometry(body.geometry))
                * 100
            )
            print(a)
            image[CustomFields.intersection_pct] = Decimal(
                area(intersection(Geometry(image["geometry"]), Geometry(body.geometry)))
                / area(Geometry(body.geometry))
                * 100
            )
        else:
            image[CustomFields.intersection_pct] = 0
    if body.ordering.value == CustomFields.intersection_pct.value:
        result = sorted(result, key=lambda k: k[CustomFields.intersection_pct])
    elif body.ordering.value == "-" + CustomFields.intersection_pct.value:
        result = sorted(
            result, key=lambda k: k[CustomFields.intersection_pct], reverse=True
        )
    if body.fields:
        fields = {"geometry", "intersection_pct"}.difference(
            set(body.fields.split(","))
        )
        result = [
            {k: v for k, v in image.items() if k not in fields} for image in result
        ]
    return result


if __name__ == "__main__":
    uvicorn.run(app, host="localhost", port=8000, workers=0)
